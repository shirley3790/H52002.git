(function () {

    /*
        * 加减数量
        * 删除某行
        * 全选反选
        * 总数量和总价格
        * 数据持久化:存到本地
    */

    let vm = new Vue({
        el: '#app',
        data: {
            goodlists: [{
                shop_isok: false,//店铺复选框
                shop_id: 10001,//店铺id
                shop_name: '5香麻辣店',//店铺名称
                shop_comm: [{ //店铺的商品
                    comm_isok: false,//商品复选框
                    comm_id: 1008,//商品id
                    comm_img: './images/1.png',//商品img路径
                    comm_int: '这是一家很不错的店里面的货物',//标题
                    comm_big: '259g', //规格
                    comm_price: '8998', //单价
                    num: 1, //数量
                    total: 8998,//总价
                    stock: 6
                }, {
                    comm_isok: false,
                    comm_img: './images/4.png',
                    comm_id: 1008,
                    comm_int: '这是一家很不错的店里面的货物',
                    comm_big: '259g',
                    comm_price: '1998',
                    num: 1,
                    total: 1998,
                    stock: 10
                }]
            },
            {
                shop_isok: false,
                shop_id: 10002,
                shop_name: '7香麻辣店',
                shop_comm: [{
                    comm_isok: false,
                    comm_id: 1008,
                    comm_img: './images/2.png',
                    comm_int: '这是一家很不错的店里面的货物',
                    comm_big: '259g',
                    comm_price: '4998',
                    num: 1,
                    total: 4998,
                    stock: 10
                }, {
                    comm_isok: false,
                    comm_id: 1008,
                    comm_img: './images/1.png',
                    comm_int: '这是一家很不错的店里面的货物',
                    comm_big: '259g',
                    comm_price: '8998',
                    num: 1,
                    total: 8998,
                    stock: 20
                }, {
                    comm_isok: false,
                    comm_img: './images/4.png',
                    comm_id: 1008,
                    comm_int: '这是一家很不错的店里面的货物',
                    comm_big: '259g',
                    comm_price: '1998',
                    num: 1,
                    total: 1998,
                    stock: 6
                }]
            }, {
                shop_isok: false,
                shop_id: 10003,
                shop_name: '1香麻辣店',
                shop_comm: [{
                    comm_isok: false,
                    comm_id: 1008,
                    comm_img: './images/3.png',
                    comm_int: '这是一家很不错的店里面的货物',
                    comm_big: '259g',
                    comm_price: '5998',
                    num: 1,
                    total: 5998,
                    stock: 6
                }, {
                    comm_isok: false,
                    comm_id: 1008,
                    comm_img: './images/1.png',
                    comm_int: '这是一家很不错的店里面的货物',
                    comm_big: '259g',
                    comm_price: '8998',
                    num: 1,
                    total: 8998,
                    stock: 6
                }, {
                    comm_isok: false,
                    comm_img: './images/4.png',
                    comm_id: 1008,
                    comm_int: '这是一家很不错的店里面的货物',
                    comm_big: '259g',
                    comm_price: '1998',
                    num: 1,
                    total: 1998,
                    stock: 6
                }]
            }, {
                shop_isok: false,
                shop_id: 10004,
                shop_name: '2香麻辣店',
                shop_comm: [{
                    comm_isok: false,
                    comm_img: './images/4.png',
                    comm_id: 1008,
                    comm_int: '这是一家很不错的店里面的货物',
                    comm_big: '259g',
                    comm_price: '1998',
                    num: 1,
                    total: 1998,
                    stock: 6
                }, {
                    comm_isok: false,
                    comm_id: 1008,
                    comm_img: './images/1.png',
                    comm_int: '这是一家很不错的店里面的货物',
                    comm_big: '259g',
                    comm_price: '8998',
                    num: 1,
                    total: 8998,
                    stock: 6
                }]
            }
            ],
            showMe: false, //会话框的开关
            total: 0,//总数量
            allPrice: 0//总价
        },
        //方法
        methods: {
            //功能：点击删除某行按钮，打开会话框
            remove(index, idx) {
                console.log(999);
                //点击删除按钮：打开会话框
                this.showMe = true;
                localStorage.setItem('shopindex', index);
                localStorage.setItem('goodindex', idx);
                // this.goodlists[index].shop_comm.splice(idx, 1);
            },

            //功能:点击确定按钮真正删除某行
            removeGood() {
                let index = localStorage.getItem('shopindex');
                let idx = localStorage.getItem('goodindex');
                this.goodlists[index].shop_comm.splice(idx, 1);//确定删除
                if (!this.goodlists[index].shop_comm.length) {
                    this.goodlists.splice(index, 1);//如果该商品没有商品，则删除整个店铺
                }
                this.showMe = false;//关闭会话框
            },

            //功能：点击二级复选框，控制三级复选框
            change(index) {
                this.goodlists[index].shop_comm.forEach(good => {
                    good.comm_isok = !this.goodlists[index].shop_isok;
                })
            }
            // add(index, idx) {
            //     //index:1 idx:2  第二家店铺的第3个商品数量要加1
            //     this.goodlists[index].shop_comm[idx].num++;
            //     // let num = this.goodlists[index].shop_comm[idx].num;
            //     // num++;
            //     // if (num > this.goodlists[index].shop_comm[idx].stock) {
            //     //     //不能超过库存量
            //     //     alert('库存量不足')
            //     //     this.goodlists[index].shop_comm[idx].num = this.goodlists[index].shop_comm[idx].stock;
            //     // } else {
            //     //     this.goodlists[index].shop_comm[idx].num = num;
            //     // }
            // },
            // cut(index, idx) {
            //     //index:1 idx:2  第二家店铺的第3个商品数量要加1
            //     this.goodlists[index].shop_comm[idx].num--;
            //     // let num = this.goodlists[index].shop_comm[idx].num;
            //     // num--;
            //     // if (num < 1) {
            //     //     //不能超过库存量
            //     //     alert('最少买1份')
            //     //     this.goodlists[index].shop_comm[idx].num = 1;
            //     // } else {
            //     //     this.goodlists[index].shop_comm[idx].num = num;
            //     // }
            // }
        },

        //监听器
        watch: {
            goodlists: {
                deep: true,
                handler(newval) {
                    //监听数量的变化，不能超过库存量
                    newval.forEach(item => {
                        item.shop_comm.forEach(good => {
                            //good:商品的每一项
                            if (good.num < 1) {
                                //最小买一份
                                good.num = 1;
                                alert('最少买一份')
                            } else if (good.num > good.stock) {
                                //最大买库存量
                                good.num = good.stock;
                                alert('最多买' + good.stock + '份');
                            }
                        })
                    });

                    //全选反选
                    newval.forEach(item => {
                        //item：某个店铺，三级复选框控制二级复选框
                        item.shop_isok = item.shop_comm.every(good => good.comm_isok == true);
                    })

                    //统计总数量和总价
                    let arr = [];
                    newval.forEach(item => {
                        //统计选中的商品
                        let filterarr = item.shop_comm.filter(good => good.comm_isok == true);
                        if (filterarr.length) {
                            arr.push(filterarr);
                        }

                    });

                    //计算总数量和总价
                    // console.log(arr);//[[{},{}],[{}]]
                    this.total = 0;//每次监听器有新值就先清空旧值
                    this.allPrice = 0;//每次监听器有新值就先清空旧值
                    arr.forEach(item => {
                        item.forEach(good => {
                            // console.log(good);
                            this.total += good.num * 1;
                            this.allPrice += good.num * good.comm_price;
                        })
                    })
                }
            }
        },

        //计算属性
        computed: {
            //功能:一三级复选框的互相制约
            allcheck: {
                get() {
                    //三级复选框控制一级复选框
                    let arr = [];
                    this.goodlists.forEach(item => {
                        let res = item.shop_comm.every(good => good.comm_isok == true);
                        arr.push(res)
                    })
                    return arr.every(item => item == true);
                },
                set(val) {
                    //一级复选框控制三级复选框
                    this.goodlists.forEach(item => {
                        let res = item.shop_comm.forEach(good => {
                            good.comm_isok = val;
                        })
                    })
                }
            }
        }
    });
})();