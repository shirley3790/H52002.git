import Vue from "vue";
import VueRouter from "vue-router";

//引入子路由 @指src目录
import appRight from '@/components/appRight.vue';
import appGoods from '@/components/appGoods.vue';
import appOrders from '@/components/appOrders.vue';
import appSport from '@/components/appSport.vue';
import appTech from '@/components/appTech.vue';
import appbug from '@/components/appbug.vue';//404
import appdetail from '@/components/appdetail.vue';//详情组件
import appTechdetail from '@/components/appTechdetail.vue';//详情组件

//防止同个路径相同出现的问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(VueRouter);//启用vueRouter


//路由表
const routes = [
  { //如果刷新页面就找不到路径了，设置重定向解决刷新丢失页面问题
    path: '/',
    redirect: '/users',
  },
  {
    path: '/users',//用户管理：路径
    component: appRight, //子组件
    name: 'home' //路由名字
  }, {
    path: '/goods',//用户管理
    component: appGoods,
    redirect: '/goods/sport', //重定向
    name: 'goods',
    children: [//添加子路由
      {
        path: '/goods/sport',//待会就用 /goods/sport访问appSport组件
        // path: 'sport',//待会就用 /goods/sport访问appSport组件
        component: appSport,
        name: 'sport',
        children: [
          {
            path: 'detail/:id',//:id 占位符，动态路由  /goods/sport/detail/2
            component: appdetail, //共用同一个组件
            name: 'sportdetail'
          }
        ]
      },
      {
        path: 'tech',//待会就用 /goods/tech 访问appTech组件
        component: appTech,
        name: 'tech',
        children: [
          {
            // path: 'detail/:id',//:id 占位符，动态路由  /goods/tech/detail/2
            path: 'detail',//:id 占位符，动态路由  /goods/tech/detail/2
            // path: 'detail?' + new Date(),//:id 占位符，动态路由  /goods/tech/detail/2
            // path: 'detail',//:id 占位符，动态路由  /goods/tech/detail
            component: appTechdetail, //共用同一个组件
            name: 'techdetail'
          }
        ]
      }

    ]
  }, {
    path: '/orders',//用户管理
    component: appOrders
  },
  {
    path: '/404',//用户管理
    component: appbug
  },

];

const router = new VueRouter({
  // 全局配置 router-link 标签生成的 CSS 类名
  linkActiveClass: 'active',//方案二：在路由配置里面添加linkActiveClass属性
  routes
});

export default router;
