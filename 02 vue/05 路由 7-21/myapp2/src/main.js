//主入口文件:插件的引入一般放这里
import Vue from "vue";//导入vue
import App from "./App.vue";//导入最大组件
import router from "./router";//导入路由:如果js文件刚好叫index。就可以忽略不写
import store from "./store";//导入vuex
import axios from "axios";
//是否启用警告：不提示
Vue.config.productionTip = false;
Vue.prototype.$axios = axios;//把axios挂载原型下面； this.$axios  this.$watch

new Vue({
  router,//将router注入到vue实例里面
  store,//把vuex注入到vue里面
  render: h => h(App) //模板：编译app.vue文件渲染到页面
}).$mount("#app");//挂载在#app下面
