import Vue from "vue";
import Vuex from "vuex";//引入vuex插件
import home from './modules/home';
Vue.use(Vuex);//启用插件


export default new Vuex.Store({//创建一个vuex实例。V和S都是大写
  // state: {},
  // getters: {},
  // mutations: {},
  // actions: {},
  modules: {
    home
  }
});
