export default {
    state: {//类似vue实例里面的data，用于存放数据，一般存放公共状态数据
        count: 1
    },

    getters: {//类似vue里面的computed 计算属性：只有getter属性
        total(state) {
            let toNum = '';
            if (state.count > 20) {
                toNum = '大于20了'
            } else {
                toNum = '小于20'
            }
            return toNum;
        }
    },

    mutations: {//类似我们methods方法,但是一般是写基础方法，逻辑简单，对state的数据的增删改查的方法写在这里
        increment(state, payload) {//n：payload 形参，载荷，只能写一个参数，如果要传多个数据，请写成对象的格式
            //加一
            state.count += payload;
        },
        decrement(state) {
            //减一
            state.count--;
        }
    },

    actions: {//类似我们methods方法，但是一般写业务逻辑比较复杂或异步的代码
        //官方推荐，在actions里面，调用mutations的方法
        addNum(context, payload) {//context==store
            context.commit('increment', payload);
        },
        cutNum({ commit }) {
            //解构的写法：commit()就是store下面调用mutations里面的方法；state：公共状态
            commit('decrement');
        }
    }
}

