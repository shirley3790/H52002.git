import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";//引入store并注入到vue实例中

Vue.config.productionTip = false;

new Vue({
  router,//将路由对象注入到vue实例中  this.$router.push()
  store,//注入到vue实例中，this.$store
  render: h => h(App)
}).$mount("#app");
