﻿//子路由：用户信息管理
const express = require('express');
const Router = express.Router(); //Router==app
//引入query方法进行数据库的查询
const mongo = require('../../db/mongo'); //引入mongo方法进行mongoDB的增删改查
var {
    ObjectId
} = require('mongodb');
let tokenFn = require('./token');
//引入multer模块
const multer = require('multer');
var fs = require('fs');
const {
    host
} = require('../../config.json');
/*
    用户管理：
        * 验证用户是否存在：存在就不给注册
        * 注册
        * 登陆：生成token
        * 验证token
        * 查询gid叫xx的用户
        * 修改密码
        * 删除gid为xx的用户
        * 查询所有的用户
*/

var storage = multer.diskStorage({
    //配置上传目录：目录如果没有就上传失败
    // destination: function (req, file, cb) {
    //   cb(null, 'uploads/')
    // },
    //目录无则自动创建
    destination: 'uploads/',
    //文件名的控制
    filename: function (req, file, cb) {
        // console.log(file);
        let filename = file.originalname; //3.jpg
        let arr = filename.split('.'); //[3,'jpg']
        // cb(null, file.fieldname + '-' + Date.now())
        cb(null, arr[0] + '-' + Date.now() + '.' + arr[1]);
    }
})

var upload = multer({
    storage: storage
}); //调用上面配置参数storage


//验证用户是否存在 :get请求
Router.get('/checkname', async (req, res) => {
    //async和await可以让我们用同步的写法实现异步的效果，await就是在等待结果
    try {
        let data = await mongo.find({
            colName: 'users', //集合名字
            query: req.query //查询条件
        });
        // console.log(data);//[{},{}]
        if (data.length) {
            //已存在：不允许注册
            inf = {
                code: 3000,
                flag: false,
                message: '用户已存在，不允许注册'
            }
        } else {
            inf = {
                code: 2000,
                flag: true,
                message: '允许注册'
            }
        }
        res.send(inf);

    } catch (err) {
        //捕获失败的回调:reject(err)
        let inf = {
            code: err.sqlState,
            flag: false,
            message: '查询失败'
        }
        res.send(inf);
    }
});

//注册功能: post请求
Router.post('/reg', async (req, res) => {
    try {
        let arr = [];
        arr.push(req.body);
        let result = await mongo.create('users', arr);
        let inf = {};
        if (result.insertedCount) {
            //注册成功
            inf = {
                code: 2000,
                flag: true,
                message: '注册成功'
            }
        } else {
            //注册失败
            inf = {
                code: 3000,
                flag: false,
                message: '注册失败'
            }
        }
        res.send(inf); //响应
    } catch (err) {
        let inf = {
            code: err.sqlState,
            flag: false,
            message: '查询失败'
        }
        res.send(inf);
    }

});

//登陆功能：get请求 http://localhost:8010/user/login
Router.get('/login', async (req, res) => {
    let inf = {};
    try {
        console.log(req.query);
        let data = await mongo.find({ //用mongo的find方法查询账号和密码是否正确
            colName: 'users',
            query: req.query
        });
        // console.log(data, 456);
        let token = tokenFn.create(req.query.password);
        // console.log(token);
        if (data.length) {
            //登陆成功
            inf = {
                code: 2000,
                flag: true,
                message: '登陆成功',
                token, //token:token Es6
                role: data[0].role,
                uid: data[0]._id
            }
        } else {
            //登陆失败
            inf = {
                code: 3000,
                flag: false,
                message: '登陆失败，账号或密码错误'
            }
        }
        // res.send(inf);//响应
    } catch (err) {
        inf = {
            code: 5000,
            flag: false,
            message: '查询失败'
        }

    }

    res.send(inf);
});

//校验token:get请求
Router.get('/verify', async (req, res) => {
    let inf = {};
    try {
        let {
            token
        } = req.query; //解构
        // console.log(req.query);
        // console.log(token, 888);
        let res = tokenFn.verify(token);
        // console.log(res, 999);
        if (res) {
            //校验通过:就允许进入购物车、个人中心
            inf = {
                code: 2000,
                flag: true,
                message: '校验通过'
            }
        } else {
            //校验失败:跳回登陆页:被改了，或过期了
            inf = {
                code: 3000,
                flag: false,
                message: '校验失败'
            }
        }
        // res.send(inf);//响应
    } catch (err) {
        inf = {
            code: 5000,
            flag: false,
            message: '校验失败' //失败：调用verify接口出错
        }

    }

    res.send(inf);
});


//修改密码：put请求  UPDATE userinf SET name='春哥',psw='666' WHERE uid=35
Router.put('/edit/:id', async (req, res) => {
    //async和await可以让我们用同步的写法实现异步的效果，await就是在等待结果
    try {
        let {
            id
        } = req.params;
        let _id = ObjectId(id); //把前端传过来的id包装一下，包装成和数据库的_id一致才能比较
        // console.log(_id);//打印不出那个效果，但是是可以使用的
        let result = await mongo.update('users', {
            _id
        }, {
            $set: req.body
        })
        if (result.result.n) {
            //修改成功
            inf = {
                code: 2000,
                flag: true,
                message: '修改成功'

            }
        } else {
            inf = {
                code: 3000,
                flag: false,
                message: '修改失败'
            }
        }
        res.send(inf);

    } catch (err) {
        //捕获失败的回调:reject(err)
        let inf = {
            code: 5000,
            flag: false,
            message: '查询失败'
        }
        res.send(inf);
    }
    // res.send('7777');
});

//功能：查询用户列表
Router.get('/list', async (req, res) => {
    //async和await可以让我们用同步的写法实现异步的效果，await就是在等待结果
    // console.log(req.query);
    let arr = [];
    let {
        search,
        page,
        pagesize
    } = req.query;
    // console.log(search, page, pagesize);
    search = JSON.parse(search);
    // console.log(search);
    let searchMap = {};
    for (let i in search) {
        if (search[i]) {
            //不为空
            var reg = new RegExp(search[i], 'i');
            searchMap[i] = reg;
        }
    }
    // console.log(searchMap);
    // let show = arr.every(item => item == 2);
    // let searchMap = show ? {} : search;
    // console.log(searchMap);
    try {
        let data = await mongo.find({
            colName: 'users',
            query: searchMap,
            page: Number(page),
            pagesize: Number(pagesize)
        });
        // console.log(data, 778);
        let alldata = await mongo.find({
            colName: 'users',
            query: searchMap
        });
        // console.log(alldata, 456);
        let pages = alldata.length > pagesize ? Math.ceil(alldata.length / pagesize) : 1;
        if (data.length) {
            //成功获取列表
            inf = {
                code: 2000,
                flag: true,
                message: '获取列表数据成功',
                total: alldata.length,
                page,
                pagesize,
                pages,
                data
            }
            // console.log(inf, 789);
        } else {
            inf = {
                code: 3000,
                flag: false,
                message: '获取列表数据失败'
            }
        }
        res.send(inf);

    } catch (err) {
        //捕获失败的回调:reject(err)
        let inf = {
            code: err.sqlState,
            flag: false,
            message: '查询失败'
        }
        res.send(inf);
    }
});

//功能：添加新用户
Router.post('/add', upload.array('files', 1), async (req, res) => {

    try {
        let url = host + '/uploads/' + req.files[0].filename;

        let arr = [];
        // console.log(req.body, 785);
        arr.push(req.body);
        // console.log(arr, 741);
        arr[0].pic = url; //添加图片
        let result = await mongo.create('users', arr);
        console.log(result, 456);
        let inf = {};
        if (result.insertedCount) {
            //添加成功
            inf = {
                code: 2000,
                flag: true,
                message: '添加成功'
            }
        } else {
            //添加失败
            inf = {
                code: 3000,
                flag: false,
                message: '添加失败'
            }
        }
        res.send(inf); //响应

    } catch (err) {
        //捕获失败的回调:reject(err)
        let inf = {
            code: 5000,
            flag: false,
            message: '查询失败'
        }
        res.send(inf);
    }
    // res.send('7777');
});

//功能：查询某个用户
Router.get('/getuser/:id', async (req, res) => {
    let {
        id
    } = req.params;
    // console.log(id, 555);
    try {
        let data = await mongo.find({
            colName: 'users', //集合名字
            query: {
                _id: ObjectId(id)
            } //查询条件
        });
        // console.log(data, 778);
        if (data.length) {
            //成功获取列表
            inf = {
                code: 2000,
                flag: true,
                message: '查询成功',
                data: data[0]
            }
        } else {
            inf = {
                code: 3000,
                flag: false,
                message: '查询失败'
            }
        }
        res.send(inf);
    } catch (err) {
        //捕获失败的回调:reject(err)
        let inf = {
            code: err.sqlState,
            flag: false,
            message: '查询失败'
        }
        res.send(inf);

    }
});

//功能:修改用户信息
Router.put('/editinf', upload.array('files', 1), async (req, res) => {
    try {
        let {
            _id
        } = req.body;
        console.log(req.body, 788);
        //上传图片拼接
        let data = await mongo.find({
            colName: 'users', //集合名字
            query: {
                _id: ObjectId(_id)
            } //查询条件
        });

        // let pics = data[0].pic;
        // console.log(pics, 11);
        // pics.push()
        // let urlList = [];
        let url = data[0].pic;
        if (req.files.length) {
            // console.log(555);
            url = host + '/uploads/' + req.files[0].filename;
        }
        // console.log(pics, 222);

        let result = await mongo.update('users', {
            _id: ObjectId(req.body._id)
        }, {
            $set: {
                "username": req.body.username,
                "name": req.body.name,
                "sex": req.body.sex,
                "birthday": req.body.birthday,
                "phone": req.body.phone,
                "address": req.body.address,
                'pic': url
            }
        });
        // console.log(result, 741);
        if (result.result.n) {
            //修改成功
            console.log('修改成功');
            inf = {
                code: 2000,
                flag: true,
                message: '修改成功'
            }
        } else {
            inf = {
                code: 3000,
                flag: false,
                message: '修改失败'
            }
        }
        res.send(inf);

    } catch (err) {
        //捕获失败的回调:reject(err)
        let inf = {
            code: 5000,
            flag: false,
            message: '查询失败'
        }
        res.send(inf);
    }
});

//功能:删除用户
Router.delete('/del/:id', async (req, res) => {
    try {
        let {
            id
        } = req.params;
        // console.log(id, 456);

        //修改内容
        let result = await mongo.remove('users', {
            _id: ObjectId(id)
        });
        // console.log(result);
        if (result.result.n) {
            //删除成功
            inf = {
                code: 2000,
                flag: true,
                message: '删除成功'
            }
        } else {
            inf = {
                code: 3000,
                flag: false,
                message: '删除失败'
            }
        }
        res.send(inf);

    } catch (err) {
        //捕获失败的回调:reject(err)
        let inf = {
            code: 5000,
            flag: false,
            message: '查询失败'
        }
        res.send(inf);
    }
})


//功能：删除已上传图片 
Router.delete('/delimg', async (req, res) => {
    let {
        id,
        url
    } = req.body;
    try {

        //获取uid，查询是否已经有项目列表
        // let data = await mongo.find({
        //     colName: 'users', //集合名字
        //     query: {
        //         _id: ObjectId(id)
        //     } //查询条件
        // });

        // let pics = data[0].pic;
        // let newpics = pics.filter(item => item != url);

        //修改内容
        let result = await mongo.update('users', {
            _id: ObjectId(id)
        }, {
            $set: {
                pic: ''
            }
        });
        // console.log(result);
        if (result.result.n) {
            //删除成功
            inf = {
                code: 2000,
                flag: true,
                message: '删除成功'
            }
            //删除路径的同时，删除本地文件 http://localhost:8020/uploads/8-1594541548620.png
            let rmurl = url.split(':')[2].split('/')[2];
            // console.log(rmurl, 999);
            // var filepath = '../../uploads/' + rmurl;
            // console.log(filepath);
            const p = require("path");
            let path = p.join(__dirname, "../../uploads/" + rmurl);
            // console.log(path);
            fs.unlink(path, function (err) {
                if (err) {
                    throw err;
                }
                console.log('文件:' + path + '删除成功！');
            })
        } else {
            inf = {
                code: 3000,
                flag: false,
                message: '删除失败'
            }
        }
        res.send(inf);

    } catch (err) {
        //捕获失败的回调:reject(err)
        let inf = {
            code: 5000,
            flag: false,
            message: '查询失败'
        }
        res.send(inf);
    }
})


module.exports = Router; //导出路由对象