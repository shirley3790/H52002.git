﻿let mongo = require('./mongo');
//引入你要插入的数据 data.json(这里保持是json数据即可)
// let data = require('../data/data.json');
const Mock = require('mockjs');

const data = Mock.mock({
    "datalist|60": [{
        "username": "@last",
        "name": "@cname",
        "birthday": "@date",
        "sex|1": ["男", "女"],
        "phone": /1[385][1-9]\d{8}/,
        "address": "@county(true)",
        "pic": "@image(50x50)"
    }]
})
// stringify(数据, 数据转换函数，缩进空格数)

// console.log(JSON.stringify(data, null, 2))
//查看效果，执行命令 node demo1.js

mongo.create('users', data.datalist); //插入数据到数据库里面(数组格式数据)