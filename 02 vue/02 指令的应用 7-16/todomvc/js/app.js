(() => {
	/*
		todoMVC
			* 渲染任务列表
			* 输入内容回车提交任务
			* 点击复选框：完成任务
			* 复选框：全选全不选
			* 双击编辑任务项
			* 删除任务项
			* 统计未完成条数
			* 切换不同的状态：数据切换
			* 清除所有完成任务项
			* 数据的存储
	*/

	//1.渲染任务列表
	// let taskList = [
	// 	{
	// 		id: 1,
	// 		title: '今天不吃饭',
	// 		ischeck: false //false：未完成
	// 	}, {
	// 		id: 2,
	// 		title: '北方同事喜欢甜粽',
	// 		ischeck: true //完成
	// 	}, {
	// 		id: 3,
	// 		title: '南方同事喜欢嫌粽',
	// 		ischeck: false
	// 	}
	// ];

	let key = 'mydata';
	let storage = {
		get() {//获取本地存储数据；取出来转成对象
			let res = localStorage.getItem(key);//如果左边为真不再执行右边
			if (res) {
				//不为空
				res = JSON.parse(res)
			} else {
				res = [];
			}
			return res;
		},
		set(data) {//存入之前转成字符串
			localStorage.setItem(key, JSON.stringify(data));
		}
	}

	// storage.set(taskList);

	let vm = new Vue({
		el: '.todoapp',//挂载点
		data: {
			taskList: storage.get(), //taskList:taskList ES6简写
			msg: '',//输入框属性
			currentIndex: null,//当前被双击的那行
			status: 'all'
		},
		//方法
		methods: {
			//功能:输入内容回车提交任务
			add() {
				console.log(this.msg);
				if (this.msg) {//数据为空就不会添加
					let obj = {//创建对象
						id: this.taskList.length + 1,
						title: this.msg,
						ischeck: false
					}
					this.taskList.push(obj);//添加任务项
					this.msg = '';
				}

			},

			//功能:双击编辑任务
			edit(index) {
				// console.log(index);
				this.currentIndex = index;
			},

			//功能：失去焦点的时候，移除编辑框,如果该任务项内容已经为空，就删除该任务项
			keepItem(index) {
				this.currentIndex = null;
				console.log(index, 999);
				if (!this.taskList[index].title) {
					//如果内容已经为空
					this.taskList.splice(index, 1);
				}
			},

			//功能：删除一条任务项
			delItem(index) {
				let istrue = confirm('您确定不要我了吗?');
				if (istrue) {
					this.taskList.splice(index, 1);
				}
			},

			//功能:删除所有已完成任务 true
			removeall() {
				this.taskList = this.taskList.filter(item => item.ischeck == false);
			}
		},
		//计算属性
		computed: {
			//功能：复选框：全选全不选
			allCheck: {
				get() {
					return this.taskList.every(item => item.ischeck == true);
				},
				set(val) {//获取allcheck的值
					// console.log(val);
					this.taskList.forEach(item => {
						item.ischeck = val;
					});
				}
			},

			//功能:统计未完成的总条数：getter方法
			total() {
				return this.taskList.filter(item => item.ischeck == false).length;
			},

			//功能：过滤数据的计算属性
			filterList() {
				switch (this.status) {
					case ''://返回所有数据
						return this.taskList;
						break;
					case 'active'://返回未完成数据
						return this.taskList.filter(item => item.ischeck == false);
						break;
					case 'completed'://返回已完成数据
						return this.taskList.filter(item => item.ischeck == true);
						break;
				}
			},

			//功能:如果当前页面没有数据，隐藏footer
			nowLen() {
				return this.taskList.length;
			}
		},
		//监听器
		watch: {
			taskList: {
				deep: true,//深度监听
				handler(newval) {
					// console.log(newval);
					storage.set(newval);//只要数据有变化，就存储一次数据
				}
			}
		},

		//注册局部指令
		directives: {
			'focus': {
				inserted(el) {
					el.focus();//底层的方法，没有兼容问题
				}
			},
			'focus2': {
				update(el) {
					el.focus();//底层的方法，没有兼容问题
				}
			}
		}
	})

	window.onhashchange = function () {
		//哈希值发生改变的时候，这里就会触发
		let hash = location.hash.slice(2);
		console.log('你变了', hash);
		vm.status = hash;//把哈希，路由状态存入data，一会就可以用于数据的过滤
	}

	window.onhashchange()//一进入页面就立马获取哈希值
})();