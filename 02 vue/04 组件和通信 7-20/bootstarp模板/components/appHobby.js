let appHobby = {
    template: `<div>
                    <h1 class="page-header">
                        <!--留一个插槽:匿名插槽 default -->
                        <!--<slot />-->
                        <!--具名插槽 default -->
                        <slot name="aaa"></slot>
                        <slot name="default"></slot>
                    </h1>
                    <div class="row placeholders">
                    <div v-for="(item, index) in hobbies" class="col-xs-6 col-sm-3 placeholder">
                        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200"
                        height="200" class="img-responsive" alt="Generic placeholder thumbnail">
                        <h4>{{ item }}</h4>
                        <span class="text-muted"><a @click="remove(index)" href="###">删除</a></span>
                    </div>
                    </div>
        </div>`,
    //3.在子组件里面用props接收数据
    props: ['hobbies', 'remove'], //props类似data，存放数据的地方(存放别的组件传过来的数据的地方)
    // props: {
    //     hobbies: Array
    // }
    // props: {
    //     hobbies: {
    //         type: Array,//数据类型规定
    //         required: true,//是否是必填项
    //         default: []//设置默认值:如果传参就用传参值(配置参数),没有配置参数才用这个默认参数
    //     }
    // }
    methods: {

    }
}