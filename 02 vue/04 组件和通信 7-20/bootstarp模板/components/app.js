
//组件模板一定要有一个且只有一个根节点
//1.注册：局部注册
let app = {
    template: `<div>
        <app-head></app-head>
        <!--核心区域:分左右两边-->
        <app-main></app-main>
    </div>`,
    components: {//注册子组件
        // 'app-head': appHead
        // 'appHead' : appHead
        appHead,
        appMain
    }
}