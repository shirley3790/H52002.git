//ts:基本类型
let num = 8;
// num = "99";//ts校验:类型不一致就出错
num = 88;

let count: number = 77;

// count = true;//出错
count = 67;

//联合类型
let age: number | string = 18;
// let age: number | string = "18";

age = "12";

type ageType = number | string;

interface obj2 {
  age: ageType;
}

let obj: obj2 = {
  age: 8,
};

//接口：使用最多
interface Iuser {
  username: string;
  age: number;
}

let laoxie: Iuser = { username: "laoxie", age: 18 }; //laoxie必须是对象，里面的username必须是字符串，age必须是数字类型

//数组
let arr: number[] = [77, 88]; //arr必须是数组，且里面的内容必须是数字类型

//泛型
let arr2: Array<string> = ["laoxie", "lemon", "jingjing"]; //arr2必须是数组，且里面的内容必须是字符串类型

//元组
let arr3: [number, number, string] = [10, 20, "h5"]; //arr3是数组，并且第一第二个值是：数字；第三个:字符串
