//index.js
//获取应用实例
const app = getApp();//关联根组件和子组件，关联后能获取根组件的数据和方法
console.log(app,899)

Page({
  //存放数据
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    name:'春哥-16k*16薪',
    array: [1, 2, 3, 4, 5],
    dataList : []
  },
  
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },

  //加载页面的时候触发
  onLoad: function () {
    console.log('index加载页面的时候触发-onLoad')
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }

    //发起ajax：获取数据  method=baidu.ting.billboard.billList&type=1&size=10&offset=0
    wx.request({
      url: 'http://tingapi.ting.baidu.com/v1/restserver/ting',
      data : {
        method : 'baidu.ting.billboard.billList',
        type : 1,
        size: 10,
        offset : 0
      },
      success:res=> {
        // console.log(res,999);
        this.setData({
          dataList : res.data.song_list
        })
      }
    })
  },

  showName() {
    console.log('调用了我')
  },
  onShow() {
    console.log('index进入页面-onShow')
  },

  onHide() {
    console.log('index离开页面-onHide')
  },

  onReady() {
    console.log('index页面渲染完成-onReady')
  },

  onPullDownRefresh() {
    //重新获取第一页的数据
    console.log('index下拉刷新页面，获取最新数据-onPullDownRefresh')
  },

  onReachBottom() {
    //上拉加载下一页
    console.log('index上拉懒加载，加载下一页-onReachBottom')
  },

  onShareAppMessage(res) {
    //分享给朋友的个性化定制
    console.log(res);
    return {
      title : '七夕节快乐，你还记得我吗',
      path: '/pages/index/index',
      imageUrl : 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1746916875,711609386&fm=26&gp=0.jpg'
    }
  },

  onPageScroll() {
    console.log('滚动到底部，加载下一页');
    
  },

  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
