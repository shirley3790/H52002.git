//index.js
//获取应用实例
const app = getApp(); //关联根组件和子组件，关联后能获取根组件的数据和方法
console.log(app, 899);

Page({
  //存放数据
  data: {
    dataList: [],
    keyword: "", //待会ajax获取最热的歌曲显示，吸引别人点击搜索
    tabs: app.globalData.types, //[{},{}]
    tabWidth: 0, //tab的下划线
    sliderOffset: 0, //下划线的初始位置
    activeIndex: 0, //当前第几个tab
    tabData: {}, //用於存放tab的數據  {1:[]}
    classicSong : [],//经典老歌
    loveSong : []//情歌对唱
  },

  //封装ajax调用
  // getList(type,size=10) {
  //   return new Promise((resolve, reject) => {
  //     wx.request({
  //       url: "http://tingapi.ting.baidu.com/v1/restserver/ting",
  //       data: {
  //         method: "baidu.ting.billboard.billList",
  //         type,
  //         size,
  //         offset: 0,
  //       },
  //       success: (res) => {
  //         // console.log(res,999);
  //         // if(type == 1) {
  //         //   this.setData({
  //         //     dataList : res.data.song_list
  //         //   })
  //         // }
  //         // cb(res.data.song_list);
  //         resolve(res.data.song_list);
  //       },
  //       fail: (err) => {
  //         reject(err);
  //       },
  //     });
  //   });
  // },

  handlerTabChange(e) {
    //点击选项的时候，tab切换
    console.log("点了tab", e);
    // let {tabWidth,tabs} = this.data;//方案一：获取点击的那个type
    let { tabWidth } = this.data;
    // let idx = e.currentTarget.dataset.idx;
    let { idx, type } = e.currentTarget.dataset; //方案二：获取点击的那个type
    let sliderOffset = tabWidth * idx;
    this.setData({
      sliderOffset,
      activeIndex: idx,
    });

    //发送ajax获取对应类型的数据
    // console.log(tabs[idx].type,'全部类型')
    // console.log(type, "全部类型2");
    //2.點擊選項卡的時候獲取最新的數據渲染
    app.getList(type).then((res) => {
      // console.log(res, "promise返回的");
      let obj = {};
      obj[type] = res;
      this.setData({
        tabData: obj,
      });
    });
  },

  //加载页面的时候触发
  onLoad: function () {
    console.log("index加载页面的时候触发-onLoad");
    
    //计算tab的下划线的宽度
    let sys = wx.getSystemInfoSync();
    // console.log(sys, 567);
    let tabWidth = sys.windowWidth / 5;
    this.setData({
      tabWidth,
    });

    //1.获取轮播图数据
    //发起ajax：获取数据  method=baidu.ting.billboard.billList&type=1&size=10&offset=0
    // this.getList(1,res => {
    //   console.log(res,'回调返回的数据')
    //   this.setData({
    //         dataList : res
    //       })
    // });
    //1.获取轮播图数据
    /*
      小程序默認不支持async await ：
        1.工具设置支持npm；
        2.去到根目录下载:npm init -y ;npm i regenerator-runtime -S
        3.构建npm：工具-构建npm
        4.const regeneratorRuntime = require('regenerator-runtime');
    */

    app.getList(1,5).then((res) => {
      console.log(res, "promise返回的");
      let obj = {};
      obj[1] = res;
      this.setData({
        dataList: res,
        tabData: obj,
      });
    });

    //获取热歌榜
    app.getList(2).then(res => {
      // console.log(res,'热歌榜');
      res.sort((a,b) => {
        return b.hot - a.hot;
      });
      // console.log('排序后', res);
      // res.forEach(item => {
      //   console.log(item.hot);
      // });
      this.setData({
        keyword : res[0].title
      })
    });

    //获取classicSong - 经典老歌
    app.getList(22).then(res => {
      // console.log(res,'经典老歌')
      this.setData({
        classicSong : res
      });
    });

    //获取loveSong - 情歌对唱
    app.getList(23).then(res => {
      // console.log(res,'情歌对唱')
      this.setData({
        loveSong : res
      });
    });

    // let arr = [2,3,1];
    // arr.sort((a,b) => {
    //   return a - b;
    // });

    // console.log(arr,'测试排序')
  },

  gotoPlay(e) {
    //编程式导航：跳转到播放页
    // console.log(e, 789);
    wx.navigateTo({
      url: "/pages/player/player?id=" + e.currentTarget.dataset.id,
    });
  },

  gotoSearch() {
    //跳转到搜索页，并且把歌名传过去
    wx.navigateTo({
      url: "/pages/search/search?keyword=" + this.data.keyword,
    });
  },

  onShow() {
    console.log("index进入页面-onShow");
  },

  onHide() {
    console.log("index离开页面-onHide");
  },

  onReady() {
    console.log("index页面渲染完成-onReady");
  },

  onPullDownRefresh() {
    //重新获取第一页的数据
    console.log("index下拉刷新页面，获取最新数据-onPullDownRefresh");
  },

  onReachBottom() {
    //上拉加载下一页
    console.log("index上拉懒加载，加载下一页-onReachBottom");
  },

  onShareAppMessage(res) {
    //分享给朋友的个性化定制
    console.log(res);
    return {
      title: "七夕节快乐，你还记得我吗",
      path: "/pages/index/index",
      imageUrl:
        "https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1746916875,711609386&fm=26&gp=0.jpg",
    };
  },

  onPageScroll() {
    console.log("滚动到底部，加载下一页");
  },

  getUserInfo: function (e) {
    console.log(e);
    app.globalData.userInfo = e.detail.userInfo;
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true,
    });
  },
});
