// pages/player/player.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info:{},//歌曲相关信息
    paused : true,//开关
    bitrate:{},//歌曲地址
    player : null
  },

  handlePlay() {
    //点击播放或暂停
    let {bitrate,paused,player} = this.data;
    //创建媒体对象
    if(!player) {//如果为空就创建一个播放器
      player = wx.createInnerAudioContext();
    }
   
    player.src = bitrate.file_link;//添加歌曲路径
    console.log(player.src);
    if(paused) {
      //播放音乐
      player.play();
    }else{
      //暂停音乐
      player.pause();
    }
    this.setData({paused:!paused,player});
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options,'这是传过来的数据')
    let {id=675017674} = options;
    //获取到歌曲的id后发送请求
    wx.request({
      url: 'http://tingapi.ting.baidu.com/v1/restserver/ting',
      data:{
        method:'baidu.ting.song.play',
        songid:id
      },
      success:res => {
        console.log(res,'歌曲信息')
        this.setData({info:res.data.songinfo,bitrate:res.data.bitrate});
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    console.log('离开播放页面了')
    let {player} = this.data;
    if(player) {
      player.pause();
    }
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})