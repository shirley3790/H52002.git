// pages/search/search.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    keyword: "", // 关键字
    inputShowed: true, // 是否显示输入框
    datalist: [], //搜索结果
  },

  showInput() {
    //显示输入框
    this.setData({
      inputShowed: true,
    });
  },

  hideInput() {
    //隐藏输入框
    this.setData({
      inputShowed: false,
      keyword: "",
    });
  },

  clearInput() {
    //清除表单
    this.setData({
      keyword: "",
    });
  },

  getList(keyword) {
    //获取音乐列表
    console.log(keyword, 888);
    wx.request({
      url: "http://tingapi.ting.baidu.com/v1/restserver/ting",
      data: {
        method: "baidu.ting.search.catalogSug",
        query: keyword,
      },
      success: (res) => {
        console.log(res);
        if (res.data.song) {
          //判断有数据再存储，防止网络问题拿不到数据，设置失败
          // let datalist = res.data.song;
          this.setData({ datalist: res.data.song });
        }
      },
    });
  },

  inputTyping(e) {
    //获取输入表单的值
    console.log("输入内容了", e);
    this.setData({
      keyword: e.detail.value,
    });

    //发送ajax请求数据:优化，节流
    this.getList(this.data.keyword);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //设置一个默认值
    let { keyword = "逆流成河" } = options;
    this.setData({
      keyword,
    });
    console.log(this.data.keyword, "搜索歌名55");
    //发送ajax请求数据
    this.getList(this.data.keyword);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {},
});
