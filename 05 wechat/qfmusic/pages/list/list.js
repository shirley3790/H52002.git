// pages/list/list.js
const app = getApp();
let {
  getList,
  globalData
} = app;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    songList: [],//列表页的数据
    page : 1,//默认加载第一页
    type : '',
    size : 10,//每次获取10条
    hasMore : true, //是否有下一页数据

  },

  getData() {
    wx.showNavigationBarLoading();//发起请求的时候打开loading
    let {type} = this.data;
    getList(type).then(res => {
      console.log(res, '列表页数据')
      this.setData({
        songList: res
      });
      wx.hideNavigationBarLoading();//获取到数据就关闭loading
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let {
      type = 22
    } = options;
    // console.log(options, '列表页获取的数据');
    this.setData({type});
    //设置页面头部
    let title = globalData.types.filter(item => item.type == type)[0].title;
    console.log(title, '页面标题获取')
    wx.setNavigationBarTitle({
      title
    });
    //发送请求，获取列表页数据
    this.getData();

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log('下拉刷新页面数据')
    let {type} = this.data;
    
    this.getData();//下拉刷新，得到最新数据，覆盖原来的页面数据
    this.setData({hasMore:false});
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log('到底部了')
    //上拉：加载下一页数据(懒加载)
    let {type,songList,size,hasMore} = this.data;
    if(!hasMore) return;
    let page = this.data.page + 1;
    // console.log(page,'加载下一页');
    console.log(type,'类型拿到了');
    getList(type,size,page).then(res => {
      console.log(res,'第' + page + '的数据');
      this.setData({
        page,
        songList : songList.concat(res),
        hasMore:res.length == size
      });
    })
    

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})